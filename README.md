Yii2 Web Font Loader Wrapper
=============

[![Latest Stable Version](https://poser.pugx.org/mzdani/yii2-webfont-loader/v/stable.svg)](https://packagist.org/packages/mzdani/yii2-webfont-loader) [![Total Downloads](https://poser.pugx.org/mzdani/yii2-webfont-loader/downloads.svg)](https://packagist.org/packages/mzdani/yii2-webfont-loader) [![Latest Unstable Version](https://poser.pugx.org/mzdani/yii2-webfont-loader/v/unstable.svg)](https://packagist.org/packages/mzdani/yii2-webfont-loader) [![License](https://poser.pugx.org/mzdani/yii2-webfont-loader/license)](https://packagist.org/packages/mzdani/yii2-webfont-loader)

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist "mzdani/yii2-webfont-loader" "dev-master"
```

or add

```
"mzdani/yii2-webfont-loader": "dev-master"
```

to the require section of your `composer.json` file.


Usage
-----

Below is some example how to use the wrapper.

```
#!php
<?php
use \mdscomp\WebFontLoader;

WebFontLoader::widget([
	'classes'  => false,
	'event'    => false,
	'timeout'  => 2000,
	'context'  => "frames['my-child']",
	'text'     => 'abcdefghijklmnopqrstuvwxyz!',
	'callback' => [
		'loading'      => 'function(){}',
		'active'       => 'function(){}',
		'inactive'     => 'function(){}',
		'fontloading'  => 'function(familyName, fvd) {}',
		'fontactive'   => 'function(familyName, fvd) {console.log(\'font name\'+familyName);}',
		'fontinactive' => 'function(familyName, fvd) {}',
	],
	'CssFontName' => 'Roboto Condensed',
	'type'        => 'google',
	'families'    => ['Roboto+Condensed:400,700,300:latin'],
]);

WebFontLoader::widget([
	'CssFontName' => 'Roboto Condensed',
	'type'        => 'typekit',
	'families'    => 'xxxx',
]);

WebFontLoader::widget([
	'CssFontName' => 'Roboto Condensed',
	'type'        => 'fontdeck',
	'families'    => 'xxxx',
]);

WebFontLoader::widget([
	'CssFontName' => 'Roboto Condensed',
	'type'        => 'monotype',
	'families'    => [
		'id'  => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
		'ver' => 12345
	],
]);

?>
```

You must use ```CssFontName``` because the script automatically create ```<style>``` to call the font. I use it so I can dynamically change font without editing css file.