<?php
/**
 * @project     Yii2 Web Font Loader Wrapper
 * @filename    WebFontLoaderAssets.php
 * @author      Mirdani Handoko <mirdani.handoko@gmail.com>
 * @copyright   copyright (c) 2015-2016, Mirdani Handoko
 * @license     BSD-3-Clause
 */

namespace mdscomp\asset;

class WebFontLoaderAsset extends \yii\web\AssetBundle {
	/**
	 * @inheritdoc
	 */
	public $sourcePath = __DIR__;

	/**
	 * @inheritdoc
	 */
	public $js = [
		'webfont.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
	];
}
