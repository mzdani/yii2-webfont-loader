<?php
/**
 * @project     Yii2 Web Font Loader Wrapper
 * @filename    WebFontLoader.php
 * @author      Mirdani Handoko <mirdani.handoko@gmail.com>
 * @copyright   copyright (c) 2015-2016, Mirdani Handoko
 * @license     BSD-3-Clause
 */

namespace mdscomp;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use mdscomp\asset\WebFontLoaderAsset;

class WebFontLoader extends Widget {
	public    $classes     = true;
	public    $event       = true;
	public    $type        = false;
	public    $families    = false;
	public    $CssFontName = false;
	public    $text        = false;
	public    $timeout     = 2000;
	public    $context     = false;
	public    $custom      = false;
	public    $callback    = false;
	protected $config      = null;

	public function init() {
		parent::init();
	}

	public function run() {
		$this->Config();
		$this->registerJS();
		$this->registerCSS();

		WebFontLoaderAsset::register($this->view);
	}

	protected function Config() {
		$conf = [];

		if (!$this->classes) {
			$conf['classes'] = (bool)$this->classes;
		}

		if (!$this->event) {
			$conf['event'] = (bool)$this->event;
		}

		if ($this->context) {
			$conf['context'] = $this->context;
		}

		if ($this->timeout !== 2000) {
			$conf['timeout'] = $this->timeout;
		}

		$fontType   = $this->Type();
		$fontFamily = $this->Families();

		$conf[$fontType] = $fontFamily;

		if ($this->text) {
			$conf[$fontType]['text'] = $this->text;
		}

		if(is_array($this->callback)){
			foreach($this->callback as $key => $value){
				$conf[$key] = new JsExpression(Html::decode($value));
			}
		}

		$this->config = $conf;

	}

	protected function Type() {
		$type = [];
		if ($this->type) {
			$type = $this->type;
		} else {
			$this->type = $type = 'google';
		}

		return $type;
	}

	protected function Families() {
		$font = [];
		switch ($this->type) {
			case 'google':
				$font['families'] = ($this->families !== false) ? $this->families : ["Roboto:100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,900,900italic:latin-ext,vietnamese,greek,cyrillic-ext,greek-ext,latin,cyrillic"];
				break;
			case 'monotype':
				$font['projectId'] = $this->families['id'];
				if (isset($this->families['ver'])) {
					$font['version'] = (int)$this->families['ver'];
				}
				break;
			case 'fontdeck':
			case 'typekit':
				$font['id'] = $this->families;
				break;
		}

		return $font;
	}

	protected function registerJS() {
		$js     = [];
		$js[]   = 'if ( typeof WebFont == "object" && typeof WebFont.load == "function" ) {';
		$js[]   = 'WebFontConfig = '.Json::encode($this->config).';';
		$js[]   = 'WebFont.load(WebFontConfig);';
		$js[]   = '}';
		$output = implode("\n", $js);

		$this->getView()->registerJs(new JsExpression(Html::decode($output)));
	}

	protected function registerCSS() {
		$css    = [];
		$css[]  = 'html body * {font-family: "'.$this->CssFontName.'", sans-serif !important;}';
		$output = implode("\n", $css);

		$this->getView()->registerCss(Html::decode($output));
	}
}